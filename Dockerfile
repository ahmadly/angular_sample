FROM      node:lts-alpine AS node
ARG       configuration=production
WORKDIR   /opt
ENV       CHROME_BIN=/usr/bin/chromium-browser
ENV       CHROME_DRIVER=/usr/bin/chromedriver

RUN       apk add     --no-cache  build-base make gcc g++ libc-dev libffi-dev musl python3 python2 chromium-chromedriver
RUN       npm install --silent --global @angular/cli

COPY      package.json        .
COPY      package-lock.json   .

RUN       npm install
COPY      .                 .

RUN       ng build --configuration=$configuration --aot=true --buildOptimizer=true --commonChunk=true --extractLicenses=true --optimization=true


FROM      nginx:alpine
RUN       rm -rf /usr/share/nginx/html/*
COPY      --from=node /opt/dist /usr/share/nginx/html
COPY      conf.nginx /etc/nginx/conf.d/default.conf

EXPOSE    80
